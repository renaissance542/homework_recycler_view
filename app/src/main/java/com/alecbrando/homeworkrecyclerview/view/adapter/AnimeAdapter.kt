package com.alecbrando.homeworkrecyclerview.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.alecbrando.homeworkrecyclerview.databinding.ListItemBinding
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.alecbrando.homeworkrecyclerview.view.loadImage
import com.bumptech.glide.Glide

class AnimeAdapter(
    private val itemClicked: (data: Data) -> Unit
): RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder>( ){

    private lateinit var data: List<Data>

    class AnimeViewHolder(
        private val binding: ListItemBinding
        ): RecyclerView.ViewHolder(binding.root) {

        fun apply(anime: Data){
            binding.tvColorHex.text = anime.attributes.slug
            binding.ivPoster.loadImage(anime.attributes.posterImage.tiny)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return AnimeViewHolder(binding).apply {
            binding.root.setOnClickListener {
                itemClicked(data[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: AnimeViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun giveData(animes: List<Data>) {
        this.data = animes
    }

    fun ImageView.loadImage(url: String) {
        Glide.with(context).load(url).into(this)
    }
}