package com.alecbrando.homeworkrecyclerview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.alecbrando.homeworkrecyclerview.databinding.FragmentDetailBinding
import com.bumptech.glide.Glide

class DetailFragment: Fragment() {

    private var _binding:FragmentDetailBinding? = null
    private val binding:FragmentDetailBinding get() = _binding!!
    val args by navArgs<DetailFragmentArgs>()
    // Image View
    // Rating
    // name
    // Description

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setElements()
    }

    fun setElements() = with(binding){
        ivCover.loadImage(args.anime.attributes.coverImage.tiny)
        tvTitle.text = args.anime.attributes.slug
        tvRating.text = args.anime.attributes.averageRating
        tvSynopsis.text = args.anime.attributes.synopsis

    }

    fun ImageView.loadImage(url: String) {
        Glide.with(context).load(url).into(this)
    }
}