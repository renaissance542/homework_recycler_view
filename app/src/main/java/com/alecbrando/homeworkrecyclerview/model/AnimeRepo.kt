package com.alecbrando.homeworkrecyclerview.model

import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.remote.AnimeApi
import com.alecbrando.homeworkrecyclerview.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object AnimeRepo {
    private val animeApi = object: AnimeApi {
        override fun getAnimes(actualData:Animes): Resource {
            return Resource.Success(actualData.data)
        }
    }

    suspend fun getAnimes(actualData: Animes):Resource = withContext(Dispatchers.IO) {
        delay(3000L)
        return@withContext animeApi.getAnimes(actualData)
    }
}