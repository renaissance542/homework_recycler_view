package com.alecbrando.homeworkrecyclerview.model.models

data class LinksX(
    val related: String,
    val self: String
)