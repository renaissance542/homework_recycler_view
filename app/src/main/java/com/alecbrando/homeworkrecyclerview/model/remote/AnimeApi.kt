package com.alecbrando.homeworkrecyclerview.model.remote

import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.util.Resource

interface AnimeApi {
    fun getAnimes(data: Animes): Resource
}