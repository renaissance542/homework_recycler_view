package com.alecbrando.homeworkrecyclerview.util

import com.alecbrando.homeworkrecyclerview.model.models.Data

sealed class Resource(data: List<Data>?){
    data class Success(val data: List<Data>): Resource(data)
    object Loading: Resource(null)
}

