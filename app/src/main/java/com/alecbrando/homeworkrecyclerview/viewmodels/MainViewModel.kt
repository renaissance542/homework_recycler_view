package com.alecbrando.homeworkrecyclerview.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alecbrando.homeworkrecyclerview.model.AnimeRepo
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel: ViewModel(){

    private val _animesList: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val animesList: LiveData<Resource> get() = _animesList

    private val repo = AnimeRepo

    fun getAnimes(data: Animes) {
        viewModelScope.launch(Dispatchers.Main) {
            _animesList.value = repo.getAnimes(data)
        }
    }
    // when ListFragment is instantiated it gets the data via the viewmodel
    // when the livedata is updated an observer in ListFragment tells vm to give the
    // data to the recyclerViewAdapter
}